﻿using System;
using System.Collections.Generic;
using System.Text;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;

using K2ADM = SourceCode.Security.AccountManagement;
using ADM = System.DirectoryServices.AccountManagement;
using SCWFC = SourceCode.Workflow.Client;
using System.Configuration;

namespace ExchgOOFSettingsBroker.Data
{
    /// <summary>
    /// https://github.com/OfficeDev/ews-managed-api
    /// </summary>
    [Attributes.ServiceObject("K2OOFSettings", "K2 OOF Settings", "K2 OOF Settings")]
    class K2OOFSettings
    {
	
		/// <summary>
        /// This property is required if you want to get the service instance configuration 
        /// settings in this class
        /// </summary>
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }
		
        #region Class Level Fields

        #region Private Fields
        private bool oofState = false;
        private string destinationName = string.Empty;
        private string destinationType = string.Empty;
        private string queryStatus = string.Empty;
        private bool queryFailed = false;

        #endregion

        #endregion

        #region Properties with Property Attribute

        [Attributes.Property("OOFState", SoType.Text, "OOF State", "The OOF State - true, false")]
        public bool OOFState
        {
            get { return this.oofState; }
            set { this.oofState = value; }
        }

        [Attributes.Property("QueryStatus", SoType.Text, "Status of this query", "If sucess, value = 'SUCCESS', else it will be the error message.")]
        public string QueryStatus
        {
            get { return this.queryStatus; }
            set { this.queryStatus = value; }
        }

        [Attributes.Property("DestinationName", SoType.Text, "Destination Name", "Destination User/Group")]
        public string DestinationName
        {
            get { return this.destinationName; }
            set { this.destinationName = value; }
        }

        [Attributes.Property("DestinationType", SoType.Text, "Destination Type", "User or Group")]
        public string DestinationType
        {
            get { return this.destinationType; }
            set { this.destinationType = value; }
        }

        #endregion

        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public K2OOFSettings()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region K2OOFSettings GetOOFStatus
        [Attributes.Method("GetOOFStatus", MethodType.Read, "Get OOF Status", "Get K2 OOF settings.",
            new string[] {},
            new string[] {},
            new string[] { "OOFState", "QueryStatus" })]
        public K2OOFSettings GetOOFSettings()
        {
            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                SCWFC.ConnectionSetup connSetup = new SCWFC.ConnectionSetup();
                connSetup.ParseConnectionString(ConfigurationManager.ConnectionStrings["WorkflowServer"].ConnectionString);

                wfConn.Open(connSetup);
                
                SCWFC.UserStatuses oStatus = wfConn.GetUserStatus();
                switch (oStatus)
                {
                    case SCWFC.UserStatuses.Available:
                        this.oofState = false;
                        break;

                    case SCWFC.UserStatuses.None:
                        this.oofState = false;
                        break;

                    case SCWFC.UserStatuses.OOF:
                        this.oofState = true;
                        break;
                }
                this.QueryStatus = "SUCCESS";
            }
            catch (Exception ex)
            {
                this.QueryStatus = "ERROR: " + ex.Message;
            }
            finally
            {
                wfConn.Dispose();
            }
            return this;
        }
        #endregion

        #region K2OOFSettings SetOOFStatus
        [Attributes.Method("SetOOFStatus", MethodType.Update, "Set OOF Status", "Set K2 OOF settings.",
            new string[] { "OOFState" },
            new string[] { "OOFState" },
            new string[] { "OOFState", "QueryStatus" })]
        public K2OOFSettings SetOOFSettings()
        {
            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                SCWFC.ConnectionSetup connSetup = new SCWFC.ConnectionSetup();
                connSetup.ParseConnectionString(ConfigurationManager.ConnectionStrings["WorkflowServer"].ConnectionString);

                wfConn.Open(connSetup);

                wfConn.SetUserStatus((this.OOFState) ? SCWFC.UserStatuses.OOF : SCWFC.UserStatuses.Available);

                this.QueryStatus = "SUCCESS";
            }
            catch (Exception ex)
            {
                this.QueryStatus = "ERROR: " + ex.Message;
            }
            finally
            {
                wfConn.Dispose();
            }
            return this;
        }
        #endregion

        #region K2OOFSettings GetShareListUsers
        [Attributes.Method("GetShareListUsers", MethodType.List, "Get Share List Users", "Get Share List Users",
            new string[] { },
            new string[] {  },
            new string[] { "DestinationName", "DestinationType" })]
        public List<K2OOFSettings> GetWorkTypes()
        {
            List<K2OOFSettings> result = new List<K2OOFSettings>();

            SCWFC.Connection wfConn = new SCWFC.Connection();
            try
            {
                SCWFC.ConnectionSetup connSetup = new SCWFC.ConnectionSetup();
                connSetup.ParseConnectionString(ConfigurationManager.ConnectionStrings["WorkflowServer"].ConnectionString);

                wfConn.Open(connSetup);

                SCWFC.WorkTypes workTypeList = wfConn.GetWorkTypes();
                if(workTypeList.Count > 0)
                {
                    SCWFC.WorkType wType = workTypeList[0];
                    foreach(SCWFC.Destination user in wType.Destinations)
                    {
                        K2OOFSettings o = new K2OOFSettings();
                        o.DestinationName = user.Name;
                        o.DestinationType = user.DestinationType.ToString();
                        result.Add(o);
                    }
                }

                this.QueryStatus = "SUCCESS";
            }
            catch (Exception ex)
            {
                this.QueryStatus = "ERROR: " + ex.Message;
            }
            finally
            {
                wfConn.Dispose();
            }
            return result;
        }
        #endregion

        #endregion
    }
}