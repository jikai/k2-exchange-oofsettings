﻿using System;
using System.Collections.Generic;
using System.Text;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;

using ews = Microsoft.Exchange.WebServices;
using K2ADM = SourceCode.Security.AccountManagement;
using ADM = System.DirectoryServices.AccountManagement;

namespace ExchgOOFSettingsBroker.Data
{
    /// <summary>
    /// https://github.com/OfficeDev/ews-managed-api
    /// </summary>
    [Attributes.ServiceObject("ExchgOOFSettings", "Exchange OOF Settings", "EWS broker for OOF Settings")]
    class ExchgOOFSettings
    {
	
		/// <summary>
        /// This property is required if you want to get the service instance configuration 
        /// settings in this class
        /// </summary>
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }
		
        #region Class Level Fields

        #region Private Fields
        private string oofState = "Disabled"; //Disabled, Enabled, Scheduled;
        private string internalEmailMsg = string.Empty;
        private string externalEmailMsg = string.Empty;
        private string externalSendTo = string.Empty; // All, Known, None
        private DateTime? startDateTime = null;
        private DateTime? endDateTime = null;
        private string queryStatus = string.Empty;
        private bool queryFailed = false;
        #endregion

        #endregion

        #region Properties with Property Attribute

        /// <summary>
        /// The state of the OOF settings right now.
        /// Possible values are: Disabled, Enabled and Scheduled
        /// </summary>
        [Attributes.Property("OOFState", SoType.Text, "OOF State", "The OOF State - Disabled, Enabled or Scheduled")]
        public string OOFState
        {
            get { return this.oofState; }
            set { this.oofState = value; }
        }

        /// <summary>
        /// The HTML email to be sent to internal parties
        /// </summary>
        [Attributes.Property("InternalEmailMsg", SoType.Text, "Internal HTML Email", "HTML email to be sent to internal parties")]
        public string InternalEmailMsg
        {
            get { return this.internalEmailMsg; }
            set { this.internalEmailMsg = value; }
        }

        /// <summary>
        /// The HTML email to be sent to external parties
        /// </summary>
        [Attributes.Property("ExternalEmailMsg", SoType.Text, "External HTML Email", "HTML email to be sent to external parties")]
        public string ExternalEmailMsg
        {
            get { return this.externalEmailMsg; }
            set { this.externalEmailMsg = value; }
        }

        /// <summary>
        /// Whether to send to external partiess
        /// </summary>
        [Attributes.Property("ExternalSendTo", SoType.Text, "Send To External Parties?", "All, Known or None")]
        public string ExternalSendTo
        {
            get { return this.externalSendTo; }
            set { this.externalSendTo = value; }
        }

        /// <summary>
        /// Shows the start date of the OOF setup
        /// This will only have value if OOFState = "Scheduled"
        /// </summary>
        [Attributes.Property("StartDate", SoType.DateTime, "Start Date/Time", "Start date/time for the OOF settings.")]
        public DateTime? StartDate
        {
            get { return this.startDateTime; }
            set { this.startDateTime = value; }
        }

        /// <summary>
        /// Shows the end date of the OOF setup
        /// This will only have value if OOFState = "Scheduled"
        /// </summary>
        [Attributes.Property("EndDate", SoType.DateTime, "End Date/Time", "End date/time for the OOF settings.")]
        public DateTime? EndDate
        {
            get { return this.endDateTime; }
            set { this.endDateTime = value; }
        }

        /// <summary>
        /// The state of the OOF settings right now.
        /// Possible values are: Disabled, Enabled and Scheduled
        /// </summary>
        [Attributes.Property("QueryStatus", SoType.Text, "Status of this query", "If sucess, value = 'SUCCESS', else it will be the error message.")]
        public string QueryStatus
        {
            get { return this.queryStatus; }
            set { this.queryStatus = value; }
        }

        #endregion

        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public ExchgOOFSettings()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region ExchgOOFSettings GetOOFSettings
        [Attributes.Method("GetOOFSettings", MethodType.Read, "Get OOF Settings", "Get Exchange OOF settings.",
            new string[] {},
            new string[] {},
            new string[] { "OOFState", "InternalEmailMsg", "ExternalSendTo", "ExternalEmailMsg", "StartDate", "EndDate", "QueryStatus" })]
        public ExchgOOFSettings GetOOFSettings()
        {
            string userEmail = string.Empty;
            ews.Data.ExchangeService svc = this.GetEWSService(ref userEmail);
            if (!queryFailed)
            {
                try
                {
                    ews.Data.OofSettings oofSettings = svc.GetUserOofSettings(userEmail);
                    this.OOFState = oofSettings.State.ToString();
                    if (oofSettings.State != ews.Data.OofState.Disabled)
                    {
                        this.InternalEmailMsg = oofSettings.InternalReply.Message;
                        this.ExternalSendTo = oofSettings.ExternalAudience.ToString();
                        this.ExternalEmailMsg = oofSettings.ExternalReply.Message;
                        if (oofSettings.State == ews.Data.OofState.Scheduled)
                        {
                            ews.Data.TimeWindow tw = oofSettings.Duration; // in server local time / or machine local time. need to test.
                            this.StartDate = tw.StartTime.ToLocalTime();
                            this.EndDate = tw.EndTime.ToLocalTime();
                        }
                    }
                    this.QueryStatus = "SUCCESS";
                }
                catch (Exception ex)
                {
                    this.QueryStatus = "ERROR: " + ex.Message;
                }   
            }

            return this;
        }
        #endregion

        #region ExchgOOFSettings TurnOnOOFNotificationScheduled
        [Attributes.Method("TurnOnOOFNotificationScheduled", MethodType.Update, "Turn on OOF notification schedule", "Turn on the OOF notification with schedule",
            new string[] { "InternalEmailMsg", "ExternalSendTo", "StartDate", "EndDate" },
            new string[] { "InternalEmailMsg", "ExternalSendTo", "ExternalEmailMsg", "StartDate", "EndDate" },
            new string[] { "QueryStatus" })]
        public ExchgOOFSettings TurnOnOOFNotificationScheduled()
        {
            ews.Data.OofExternalAudience sendToExternal = this.GetExternalAudienceFromString(this.ExternalSendTo);
            if(sendToExternal != ews.Data.OofExternalAudience.None && string.IsNullOrEmpty(this.ExternalEmailMsg))
            {
                throw new ArgumentNullException("ExternalEmailMsg");
            }

            string userEmail = string.Empty;
            ews.Data.ExchangeService svc = this.GetEWSService(ref userEmail);
            if (!queryFailed)
            {
                try
                {
                    ews.Data.OofSettings oofSettings = new ews.Data.OofSettings();
                    oofSettings.Duration = new ews.Data.TimeWindow(this.StartDate.Value, this.EndDate.Value);
                    oofSettings.InternalReply = new ews.Data.OofReply(this.InternalEmailMsg);
                    if(sendToExternal != ews.Data.OofExternalAudience.None)
                    {
                        oofSettings.ExternalReply = new ews.Data.OofReply(this.ExternalEmailMsg);
                        oofSettings.ExternalAudience = sendToExternal;
                    }
                    oofSettings.State = ews.Data.OofState.Scheduled;
                    svc.SetUserOofSettings(userEmail, oofSettings);
                    this.QueryStatus = "SUCCESS";
                }
                catch (Exception ex)
                {
                    this.QueryStatus = "ERROR: " + ex.Message;
                }
            }
            return this;
        }
        #endregion

        #region ExchgOOFSettings TurnOnOOFNotificationPernament
        [Attributes.Method("TurnOnOOFNotificationPernament", MethodType.Update, "Turn on OOF notification without schedule", "Turn on the OOF notification without a schedule",
            new string[] { "InternalEmailMsg", "ExternalSendTo" },
            new string[] { "InternalEmailMsg", "ExternalSendTo", "ExternalEmailMsg" },
            new string[] { "QueryStatus" })]
        public ExchgOOFSettings TurnOnOOFNotificationPernament()
        {
            ews.Data.OofExternalAudience sendToExternal = this.GetExternalAudienceFromString(this.ExternalSendTo);
            if (sendToExternal != ews.Data.OofExternalAudience.None && string.IsNullOrEmpty(this.ExternalEmailMsg))
            {
                throw new ArgumentNullException("ExternalEmailMsg");
            }

            string userEmail = string.Empty;
            ews.Data.ExchangeService svc = this.GetEWSService(ref userEmail);
            if (!queryFailed)
            {
                try
                {
                    ews.Data.OofSettings oofSettings = new ews.Data.OofSettings();
                    oofSettings.InternalReply = new ews.Data.OofReply(this.InternalEmailMsg);
                    if (sendToExternal != ews.Data.OofExternalAudience.None)
                    {
                        oofSettings.ExternalReply = new ews.Data.OofReply(this.ExternalEmailMsg);
                        oofSettings.ExternalAudience = sendToExternal;
                    }
                    oofSettings.State = ews.Data.OofState.Enabled;
                    svc.SetUserOofSettings(userEmail, oofSettings);
                    this.QueryStatus = "SUCCESS";
                }
                catch (Exception ex)
                {
                    this.QueryStatus = "ERROR: " + ex.Message;
                }
            }
            return this;
        }
        #endregion

        #region ExchgOOFSettings TurnOffOOFNotification
        [Attributes.Method("TurnOffOOFNotification", MethodType.Update, "Turn off OOF notification", "Turn off OOF notification",
            new string[] { },
            new string[] { },
            new string[] { "QueryStatus" })]
        public ExchgOOFSettings TurnOffOOFNotification()
        {
            string userEmail = string.Empty;
            ews.Data.ExchangeService svc = this.GetEWSService(ref userEmail);
            if (!queryFailed)
            {
                try
                {
                    ews.Data.OofSettings oofSettings = new ews.Data.OofSettings();
                    oofSettings.State = ews.Data.OofState.Disabled;
                    svc.SetUserOofSettings(userEmail, oofSettings);
                    this.QueryStatus = "SUCCESS";
                }
                catch (Exception ex)
                {
                    this.QueryStatus = "ERROR: " + ex.Message;
                }
            }
            return this;
        }
        #endregion

        #endregion

        #region private
        private string GetDomain(string K2UserName)
        {
            string[] userNameSplit = K2UserName.Split(new char[] { '\\' });
            if (userNameSplit.Length > 1)
                return userNameSplit[0];
            else
                return K2UserName;  
        }

        private string GetLoginName(string K2UserName)  
        {
            string[] userNameSplit = K2UserName.Split(new char[] { '\\' });
            if (userNameSplit.Length > 1)
                return userNameSplit[1];
            else
                return K2UserName;
        }

        private bool AutodiscoverRedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            if (Convert.ToBoolean(this.ServiceConfiguration["RequireHTTPS"]) == true)
            {
                Uri redirectionUri = new Uri(redirectionUrl);

                // Validate the contents of the redirection URL. In this simple validation
                // callback, the redirection URL is considered valid if it is using HTTPS
                // to encrypt the authentication credentials. 
                if (redirectionUri.Scheme == "https")
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        private ews.Data.ExchangeService GetEWSService(ref string userEmail)
        {
            if(this.ServiceConfiguration.ServiceAuthentication.AuthenticationMode == AuthenticationMode.OAuth)
            {
                throw new Exception("This broker does not work with OAuth authentication mode.");
            }

            ews.Data.ExchangeService svc = new ews.Data.ExchangeService(ews.Data.ExchangeVersion.Exchange2007_SP1, TimeZoneInfo.Local);

            string userName = this.GetLoginName(this.ServiceConfiguration.ServiceAuthentication.UserName);
            string domain = this.GetDomain(this.ServiceConfiguration.ServiceAuthentication.UserName);
            string ewsUrl = (this.ServiceConfiguration["EwsURLFallback"] == null) ? string.Empty : this.ServiceConfiguration["EwsURLFallback"].ToString();
            bool useAutoDiscover = Convert.ToBoolean(this.ServiceConfiguration["UseAutoDiscover"]);

            ADM.PrincipalContext ctx = new ADM.PrincipalContext(ADM.ContextType.Domain);
            K2ADM.ADUser user = K2ADM.ADUser.FindByIdentity(ctx, userName);
            userEmail = user.EmailAddress;

            if (this.ServiceConfiguration.ServiceAuthentication.Impersonate || this.ServiceConfiguration.ServiceAuthentication.AuthenticationMode == AuthenticationMode.ServiceAccount)
            {
                svc.UseDefaultCredentials = true;
            }
            else
            {
                string password = this.ServiceConfiguration.ServiceAuthentication.Password;
                svc.Credentials = new System.Net.NetworkCredential(userName, password);
            }

            #region try auto discover
            if (useAutoDiscover)
            {
                try
                {
                    svc.AutodiscoverUrl(userEmail, AutodiscoverRedirectionUrlValidationCallback);
                }
                catch (ews.Autodiscover.AutodiscoverRemoteException autoDiscoveryRemoteEx)
                {
                    if (!string.IsNullOrEmpty(ewsUrl))
                    {
                        svc.Url = new Uri(ewsUrl);
                    }
                    else
                    {
                        queryFailed = true;
                        this.QueryStatus = "ERROR: " + autoDiscoveryRemoteEx.Message;
                    }
                }
                catch (ews.Data.AutodiscoverLocalException autoDiscoveryLocalex)
                {
                    if (!string.IsNullOrEmpty(ewsUrl))
                    {
                        svc.Url = new Uri(ewsUrl);
                    }
                    else
                    {
                        queryFailed = true;
                        this.QueryStatus = "ERROR: " + autoDiscoveryLocalex.Message;
                    }
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(ewsUrl))
                    {
                        svc.Url = new Uri(ewsUrl);
                    }
                    else
                    {
                        queryFailed = true;
                        this.QueryStatus = "ERROR: " + ex.Message;
                    }
                }
            }
            #endregion
            return svc;
        }

        private ews.Data.OofState GetOOFStateFromString(string strOOFState)
        {
            ews.Data.OofState result = ews.Data.OofState.Disabled;
            switch(strOOFState.ToLower())
            {
                case "disabled":
                    result = ews.Data.OofState.Disabled;
                    break;

                case "enabled":
                    result = ews.Data.OofState.Enabled;
                    break;

                case "scheduled":
                    result = ews.Data.OofState.Scheduled;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("OOFState", "OOFState value can be 'Disabled', 'Enabled' or 'Scheduled' only.");
            }
            return result;
        }

        private ews.Data.OofExternalAudience GetExternalAudienceFromString(string externalSendTo)
        {
            ews.Data.OofExternalAudience result = ews.Data.OofExternalAudience.None;
            switch(externalSendTo.ToLower())
            {
                case "none":
                    result = ews.Data.OofExternalAudience.None;
                    break;

                case "all":
                    result = ews.Data.OofExternalAudience.All;
                    break;

                case "known":
                    result = ews.Data.OofExternalAudience.Known;
                    break;

                default:
                    throw new ArgumentException("ExternalSendTo value must be None, All or Known.");
            }
            return result;
        }
        #endregion
    }
}