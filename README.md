# K2 Exchange OOFSettings#

For executing EWS calls to get/update Out-of-Office settings on Exchange.

See the Deploy\README.doc for more details.